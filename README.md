## lean hugo theme
This is the lean hugo theme, a port of the [lean jekyll theme][lean-jekyll].

## Usage
Inside a hugo site, do

    $ git submodule add http://framagit.org/lafleur/lean-hugo-theme.git themes/lean

To update the theme to its latest version, inside the hugo site do

    $ cd themes/lean
    $ git pull

To update all submodules, inside the hugo site do

    $ git submodule update --remote

## Contributing
This project is intended to be a welcoming space for collaboration. If you have
an idea, suggestion, feature request, etc., feel free to open an issue or pull
request.

## License
The source code in this project is protected by the [GPLv3 license][gpl3].

[lean-jekyll]: https://gitlab.com/lafleurdeboum/lean-jekyll-theme
[gpl3]: https://opensource.org/licenses/GPL-3.0

